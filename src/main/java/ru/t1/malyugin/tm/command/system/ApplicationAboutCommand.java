package ru.t1.malyugin.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show Author info";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + getPropertyService().getApplicationName());
        System.out.println();

        System.out.println("[AUTHOR]");
        System.out.println("NAME: " + getPropertyService().getAuthorName());
        System.out.println("E-MAIL: " + getPropertyService().getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + getPropertyService().getGitBranch());
        System.out.println("COMMIT ID: " + getPropertyService().getGitCommitId());
        System.out.println("COMMITTER NAME: " + getPropertyService().getGitCommitterName());
        System.out.println("COMMITTER EMAIL: " + getPropertyService().getGitCommitterEmail());
        System.out.println("MESSAGE: " + getPropertyService().getGitCommitMessage());
        System.out.println("TIME: " + getPropertyService().getGitCommitTime());
    }

}