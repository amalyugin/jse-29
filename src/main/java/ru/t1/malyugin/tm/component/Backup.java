package ru.t1.malyugin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.command.data.load.DataBackupLoadCommand;
import ru.t1.malyugin.tm.command.data.save.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    private void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    private void load() {
        @NotNull final String backupDir = bootstrap.getPropertyService().getApplicationDumpDir();
        @NotNull final String backupFile = AbstractDataCommand.FILE_BACKUP;
        if (!Files.exists(Paths.get(backupDir + backupFile))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(5000);
            save();
        }
    }

}