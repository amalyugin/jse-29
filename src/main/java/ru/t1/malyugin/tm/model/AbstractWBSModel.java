package ru.t1.malyugin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.model.IWBS;
import ru.t1.malyugin.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractWBSModel extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    protected Date created = new Date();

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @NotNull
    protected String name = "";

}