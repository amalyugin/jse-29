package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return models.values().stream()
                .filter(u -> StringUtils.equals(login, u.getLogin()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) {
        return models.values().stream()
                .filter(u -> StringUtils.equals(email, u.getEmail()))
                .findAny()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return models.values().stream()
                .anyMatch(u -> StringUtils.equals(login, u.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return models.values().stream()
                .anyMatch(u -> StringUtils.equals(email, u.getEmail()));
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String passwordHash) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email,
            @NotNull final Role role
    ) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        user.setRole(role);
        return add(user);
    }

}